package com.worldbank.entity;

import java.util.Arrays;

public class SearchRequest {
		
	private String[] documentTypes;
	private String[] sources;
	private String[] includeKeyWords;
	private String[] excludeKeywords;
	private String page;
	
	public SearchRequest() {
	}

	public SearchRequest(String[] documentTypes, String[] sources, String[] includeKeyWords, String[] excludeKeywords,
			String page) {
		super();
		this.documentTypes = documentTypes;
		this.sources = sources;
		this.includeKeyWords = includeKeyWords;
		this.excludeKeywords = excludeKeywords;
		this.page = page;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String[] getDocumentTypes() {
		return documentTypes;
	}

	public void setDocumentTypes(String[] documentTypes) {
		this.documentTypes = documentTypes;
	}

	public String[] getSources() {
		return sources;
	}

	public void setSources(String[] sources) {
		this.sources = sources;
	}

	public String[] getIncludeKeyWords() {
		return includeKeyWords;
	}

	public void setIncludeKeyWords(String[] includeKeyWords) {
		this.includeKeyWords = includeKeyWords;
	}

	public String[] getExcludeKeywords() {
		return excludeKeywords;
	}

	public void setExcludeKeywords(String[] excludeKeywords) {
		this.excludeKeywords = excludeKeywords;
	}

	@Override
	public String toString() {
		return "SearchRequest [documentTypes=" + Arrays.toString(documentTypes) + ", sources="
				+ Arrays.toString(sources) + ", includeKeyWords=" + Arrays.toString(includeKeyWords)
				+ ", excludeKeywords=" + Arrays.toString(excludeKeywords) + "]";
	}
}
