package com.worldbank.entity;

import java.io.Serializable;

public class LoginResponse implements Serializable {

	/**
	 * Serial version uid
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean success;
	private String message;
	
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	private int statusCode;

}
