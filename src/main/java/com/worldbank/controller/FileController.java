package com.worldbank.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.worldbank.util.AppConstant;

@RestController
public class FileController {

	/**
	 * Download a file from an external path configured in AppConstant
	 * 
	 * @param request
	 * @param response
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/pdf/external/{fileName:.+}", method = RequestMethod.GET, produces = "application/pdf")
	public ResponseEntity<InputStreamResource> downloadExternal(HttpServletRequest request,
			HttpServletResponse response, @PathVariable("fileName") String fileName) throws IOException {
		File file = new File(AppConstant.pdfPath + fileName);
		if (file.exists()) {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType("application/pdf"));
			headers.add("Access-Control-Allow-Origin", "*");
			headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
			headers.add("Access-Control-Allow-Headers", "Content-Type");
			headers.add("Content-Disposition", "filename=" + fileName);
			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");

			headers.setContentLength((int) file.length());
			
			InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
			ResponseEntity<InputStreamResource> res = new ResponseEntity<InputStreamResource>(
					new InputStreamResource(inputStream), headers, HttpStatus.OK);
			return res;
		}
		return null;
	}

	/**
	 * Download a file from classpath
	 * 
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/pdf/internal/{fileName:.+}", method = RequestMethod.GET, produces = "application/pdf")
	public ResponseEntity<InputStreamResource> downloadInternal(@PathVariable("fileName") String fileName)
			throws IOException {
		ClassPathResource pdfFile = new ClassPathResource("pdf/" + fileName);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
		headers.add("Access-Control-Allow-Headers", "Content-Type");
		headers.add("Content-Disposition", "filename=" + fileName);
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		headers.setContentLength(pdfFile.contentLength());
		ResponseEntity<InputStreamResource> response = new ResponseEntity<InputStreamResource>(
				new InputStreamResource(pdfFile.getInputStream()), headers, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/chart/{data}", method = RequestMethod.GET, produces = "application/pdf")
	public ResponseEntity<InputStreamResource> downloadChart(@PathVariable("data") String data) throws IOException {

		ClassPathResource xlsxFile = new ClassPathResource("chart.xlsx");
		File fo = new File(xlsxFile.getURI());
		XSSFWorkbook a = new XSSFWorkbook(new FileInputStream(fo));
		XSSFSheet my_sheet = a.getSheetAt(0);
		XSSFRow my_row = my_sheet.getRow(0);

		XSSFCell myCell = my_row.getCell(1);
		System.out.println(myCell.getNumericCellValue());
		myCell.setCellValue(Integer.valueOf(data));

		FileOutputStream outFile = new FileOutputStream(new File(xlsxFile.getURI()));
		a.write(outFile);
		outFile.close();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
		headers.add("Access-Control-Allow-Headers", "Content-Type");
		headers.add("Content-Disposition", "filename=chart.xlsx");
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");

		headers.setContentLength(xlsxFile.contentLength());
		ResponseEntity<InputStreamResource> response = new ResponseEntity<InputStreamResource>(
				new InputStreamResource(xlsxFile.getInputStream()), headers, HttpStatus.OK);
		return response;

	}
}
