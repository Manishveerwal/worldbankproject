package com.worldbank.controller;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.util.ArrayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.worldbank.entity.SearchRequest;
import com.worldbank.restclient.HttpRestClient;

@RestController
public class SolrReqController {

	@Autowired
	private HttpRestClient httpSolrRestClient;

	@RequestMapping(value = "/search", method = RequestMethod.POST, produces = "text/plain", consumes = "application/json")
	public @ResponseBody String search(@RequestBody SearchRequest searchRequest) {

		if (!validate(searchRequest)) {
			return "Error!";
		}
		
		if (!ArrayUtils.contains(searchRequest.getSources(), "Internal Reports")) {
			return "";
		}
		
		if (!ArrayUtils.contains(searchRequest.getDocumentTypes(), "pdf")) {
			return "";
		}
		return httpSolrRestClient.call(searchRequest);
	}

	private boolean validate(SearchRequest searchRequest) {
		if (searchRequest.getIncludeKeyWords() == null || searchRequest.getIncludeKeyWords().length == 0) {
			return false;
		}
		return true;
	}
}