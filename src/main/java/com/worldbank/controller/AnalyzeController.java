package com.worldbank.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.worldbank.analyze.summarize.SummarizeEngine;
import com.worldbank.request.SummarizeTextRequest;
import com.worldbank.request.SummarizeWebRequest;
import com.worldbank.util.AppConstant;

@RestController
public class AnalyzeController {

	@Autowired
	private SummarizeEngine simpleSummaryEngine;

	@RequestMapping(value = "/summarize/text", method = RequestMethod.POST, produces = "text/plain", consumes = "application/json")
	public @ResponseBody String search(@RequestBody SummarizeTextRequest summarizeTextRequest) {

		if (summarizeTextRequest == null || StringUtils.isBlank(summarizeTextRequest.getInputText())
				|| StringUtils.isBlank(summarizeTextRequest.getSentences())) {
			return "";
		}

		return simpleSummaryEngine.getSummary(summarizeTextRequest.getInputText(),
				Integer.valueOf(summarizeTextRequest.getSentences()));
	}

	@RequestMapping(value = "/summarize/upload", method = RequestMethod.POST)
	public @ResponseBody String handleSummarizeFileUpload(@RequestParam("file") MultipartFile file) {
		String name = "test11";
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File("/home/ubuntu/files/summarize/" + name)));
				stream.write(bytes);
				stream.close();
				
				String contents = new String(Files.readAllBytes(Paths.get("/home/ubuntu/files/summarize/" + name)));
				SummarizeTextRequest summarizeTextRequest = new SummarizeTextRequest();
				summarizeTextRequest.setInputText(contents);
				summarizeTextRequest.setSentences("3");
				return search(summarizeTextRequest);
				
				//return "You successfully uploaded " + name + " into " + name + "-uploaded !";
			} catch (Exception e) {
				return "You failed to upload " + name + " => " + e.getMessage();
			}
		} else {
			return "You failed to upload " + name + " because the file was empty.";
		}
	}
	
	@RequestMapping(value = "/summarize/web", method = RequestMethod.POST, produces = "text/plain", consumes = "application/json")
	public @ResponseBody String handleSummarizeWebUrl(@RequestBody SummarizeWebRequest summarizeWebRequest) {

		if (summarizeWebRequest == null || StringUtils.isBlank(summarizeWebRequest.getWeburl())
				|| StringUtils.isBlank(summarizeWebRequest.getSentences())) {
			return "";
		}

		System.out.println(summarizeWebRequest.getWeburl());
		final String uri = "http://api.smmry.com?SM_API_KEY=44171844FA&SM_LENGTH="+summarizeWebRequest.getSentences()+"&SM_URL="+summarizeWebRequest.getWeburl();
		RestTemplate restTemplate = new RestTemplate();
	     
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	     
	    ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, entity, String.class);
	     
	    System.out.println(result);
	    return result.getBody();
	}
}
