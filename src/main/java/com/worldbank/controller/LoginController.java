package com.worldbank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.worldbank.entity.LoginRequest;
import com.worldbank.entity.LoginResponse;
import com.worldbank.security.CustomAuthenticationProvider;
import com.worldbank.util.AppConstant.STATUS_CODE;

@RestController()
public class LoginController {
	
	@Autowired CustomAuthenticationProvider authenticationManager;
	
	/**
	 * Validate login request using username and password
	 * 
	 * @param loginRequest (username and password)
	 * @return loginResponse (Status code and message)
	 */
	@RequestMapping(value="/login", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody LoginResponse login(@RequestBody LoginRequest loginRequest) {
		LoginResponse loginResponse = new LoginResponse();
		// Use custom authentication to validate login request
		final UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());
		final Authentication authentication = authenticationManager.authenticate(authRequest);
		// Check if user is authenticated or not
		if(authentication != null) {
			SecurityContextHolder.getContext().setAuthentication(authentication);
			loginResponse.setSuccess(true);
			loginResponse.setMessage("User authenticated successfully");
			loginResponse.setStatusCode(STATUS_CODE.AUTHORIZED.code);
		} else {
			loginResponse.setSuccess(false);
			loginResponse.setMessage("Invalid username or password");
			loginResponse.setStatusCode(STATUS_CODE.UNAUTHORIZED.code);
		}
		return loginResponse;
	}
}
