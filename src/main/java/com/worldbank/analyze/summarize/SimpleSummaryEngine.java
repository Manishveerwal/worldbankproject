package com.worldbank.analyze.summarize;

import org.springframework.stereotype.Service;

import net.sf.classifier4J.summariser.ISummariser;
import net.sf.classifier4J.summariser.SimpleSummariser;

@Service
public class SimpleSummaryEngine implements SummarizeEngine {

	@Override
	public String getSummary(String text, int sentences) {
		ISummariser summariser = new SimpleSummariser();
		String result = summariser.summarise(text, sentences);
		return result;
	}
}
