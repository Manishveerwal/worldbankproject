package com.worldbank.analyze.summarize;

public interface SummarizeEngine {

	String getSummary(String text, int sentences);
}
