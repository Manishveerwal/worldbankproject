package com.worldbank.util;

public class AppConstant {

	// Enum for login API status codes
	public static enum STATUS_CODE {
		AUTHORIZED(200), UNAUTHORIZED(401);
		
		public final int code;
		
		private STATUS_CODE(int code)

		{
			this.code = code;
		}
	}
	
	// Folder path for downloading pdf
	public static String pdfPath = "C:/pdf/";
}
