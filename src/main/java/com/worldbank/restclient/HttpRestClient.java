package com.worldbank.restclient;

import com.worldbank.entity.SearchRequest;

public interface HttpRestClient {
	
	String call(SearchRequest searchRequest);
}
