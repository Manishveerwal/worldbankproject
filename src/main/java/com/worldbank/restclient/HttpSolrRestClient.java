package com.worldbank.restclient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.worldbank.entity.SearchRequest;

@Component
public class HttpSolrRestClient implements HttpRestClient {
	
	private static final String BASE_URL = "http://52.221.244.52:8983/solr";
	private static final String SOLR_COLLECTION = "worldbank";
	private static final String QUERY_URL = "select";
	private static final String QUERY_PARAM = "hl.fl=fileData&hl=on&indent=on&wt=json&hl.method=fastVector&rows=9&hl.fragsize=300";

	@Override
	public String call(SearchRequest searchRequest) {

		Map<String, String> parameter = new HashMap<String, String>();
		try {
			
			StringBuilder query = new StringBuilder();
			query.append("(");
			for (int i = 0; i < searchRequest.getIncludeKeyWords().length; i++) {
				query.append(searchRequest.getIncludeKeyWords()[i]);
				
				if ( i + 1 != searchRequest.getIncludeKeyWords().length) {
					query.append("%20AND%20");
				}
			}
			query.append(")");
			parameter.put("q", query.toString());
			
			if (searchRequest.getExcludeKeywords() != null && searchRequest.getExcludeKeywords().length != 0) {
				query.setLength(0);
				query.append("(");
				for (int i = 0; i < searchRequest.getExcludeKeywords().length; i++) {
					query.append(searchRequest.getExcludeKeywords()[i]);
					
					if ( i + 1 != searchRequest.getExcludeKeywords().length) {
						query.append("%20AND%20");
					}
				}
				query.append(")");
				parameter.put("exclude", query.toString());
			}
			
			int start = Integer.valueOf(searchRequest.getPage()) * 9;
			parameter.put("start", String.valueOf(start));
			
			URL url = new URL(createURL(parameter, searchRequest));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");

			OutputStream os = conn.getOutputStream();
			os.flush();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output = "";
			System.out.println("Output from Server .... \n");
			StringBuilder resposeString = new StringBuilder();
			while ((output = br.readLine()) != null) {
				resposeString.append(output);
			}
			JSONObject jObject  = new JSONObject(resposeString.toString());
			JSONObject data = jObject.getJSONObject("response");
			JSONArray jsonarr = data.getJSONArray("docs");
			System.out.println(resposeString.toString());
			System.out.println(" Old ----- " + jsonarr);
			for(int i=0;i<jsonarr.length();i++){
			    JSONObject object = jsonarr.getJSONObject(i);
			    Map<String, String> frequencyCount = new HashMap<String, String>();
			    Iterator<String> keys = object.keys();
			    while( keys.hasNext() ) {
			        String key = (String)keys.next();
			        if (key.contains("termfreq")) {
			        	String value = object.getString(key);
			        	// Create a map for key value pair and remove this jsonobject
			        	String searchKey = key.replace("termfreq(fileData,", "");
			        	searchKey = searchKey.replace(")", "");
			        	frequencyCount.put(searchKey, value);
			        }
			    }
			    object.put("frequencyCount", frequencyCount);
			}
			System.out.println("Updated ----- " + jsonarr);
			data.put("docs", jsonarr);
			jObject.put("response", data);
			conn.disconnect();
			//return resposeString.toString();
			return jObject.toString();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private String createURL(Map<String, String> parameter, SearchRequest searchRequest) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(BASE_URL);
		stringBuilder.append("/");
		stringBuilder.append(SOLR_COLLECTION);
		stringBuilder.append("/");
		stringBuilder.append(QUERY_URL);
		stringBuilder.append("?");
		stringBuilder.append(QUERY_PARAM);
		stringBuilder.append("&");
		stringBuilder.append("start=");
		stringBuilder.append(parameter.get("start"));
		stringBuilder.append("&");
		stringBuilder.append("q=fileData:");
		stringBuilder.append(parameter.get("q"));
		
		if (parameter.get("exclude") != null) {
			stringBuilder.append("%20-fileData:");
			stringBuilder.append(parameter.get("exclude"));
		}
		stringBuilder.append("&");
		stringBuilder.append(createSelectFields(searchRequest.getIncludeKeyWords()));
		
		System.out.println(stringBuilder.toString());
		return stringBuilder.toString();
	}
	
	private String createSelectFields (String[] fields) {
		
		StringBuilder builder = new StringBuilder();
		builder.append("fl=fileName,id,score,");
		for (int i = 0; i < fields.length; i++) {
			builder.append("termfreq(fileData," + fields[i] + ")");
			if (fields.length > 1 && fields.length > i + 1) {
				builder.append(",");
			}
		}
		
		return builder.toString();
	}

	public static void main(String[] args) {
		HttpSolrRestClient client = new HttpSolrRestClient();
		client.call(null);
	}

}
