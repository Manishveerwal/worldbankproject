package com.worldbank.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	// Custom authentication
	@Autowired
    private CustomAuthenticationProvider authProvider;
 
	/**
	 * Use custom authentication provider to authenticate login request
	 */
    @Override
    protected void configure(
      AuthenticationManagerBuilder auth) throws Exception {
    	auth.authenticationProvider(authProvider);
    }
 
    /**
     * By pass security for login API to manually authenticate user
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
        .authorizeRequests()
        .antMatchers("/login").permitAll()
        .antMatchers(HttpMethod.OPTIONS, "/search").permitAll()
        .anyRequest().authenticated()
        .and()
        .httpBasic();
        
    }
}
