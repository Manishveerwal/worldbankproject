package com.worldbank.request;

public class SummarizeTextRequest {
	
	private String inputText;
	private String sentences;
	
	public SummarizeTextRequest() {
	}

	public String getInputText() {
		return inputText;
	}

	public void setInputText(String inputText) {
		this.inputText = inputText;
	}

	public String getSentences() {
		return sentences;
	}

	public void setSentences(String sentences) {
		this.sentences = sentences;
	}
}
