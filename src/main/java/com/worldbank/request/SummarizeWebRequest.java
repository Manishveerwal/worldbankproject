package com.worldbank.request;

public class SummarizeWebRequest {
	
	private String weburl;
	private String sentences;
	
	public SummarizeWebRequest() {
	}

	public String getWeburl() {
		return weburl;
	}

	public void setWeburl(String weburl) {
		this.weburl = weburl;
	}

	public String getSentences() {
		return sentences;
	}

	public void setSentences(String sentences) {
		this.sentences = sentences;
	}
}
